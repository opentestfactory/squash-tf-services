# Copyright (c) 2019-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from setuptools import setup, find_packages

with open('VERSION', 'r') as fv:
    VERSION = fv.read()
with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='squash_tf_services',
    version=VERSION,
    description='OpenTestFactory library for retrieving parameters in Robot Framework tests',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://opentestfactory.org/',
    author='Henix',
    author_email='opentestfactory@henix.com',
    packages=find_packages(),
    install_requires=[],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries',
        'License :: OSI Approved :: Apache Software License',
    ],
    license='Apache Software License (https://www.apache.org/licenses/LICENSE-2.0)',
)
