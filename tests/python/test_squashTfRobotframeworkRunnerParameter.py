# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest

from unittest.mock import MagicMock, patch

import squash_tf


class TestRFRunnerParameter(unittest.TestCase):
    """squashTfRobotframeworkRunnerParameter unit tests."""

    @classmethod
    def setUpClass(cls) -> None:
        """Patches print to avoid verbose console logs."""
        cls.print_mock = MagicMock()
        cls.print_patcher = patch('builtins.print', cls.print_mock)
        cls.print_patcher.start()

    @classmethod
    def tearDownClass(cls) -> None:
        try:
            cls.print_patcher.stop()
        except AttributeError:
            pass

    def test_initialize_service_ko(self):
        with self.print_patcher:
            squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(None)
        self.print_mock.assert_called_with(
            'No test case data pointer, falling back on default values'
        )

    def test_initialize_service_open_param_file_ko(self):
        with self.print_patcher:
            squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
                'path/xYz'
            )
        self.assertRaises(Exception)
        self.assertIn(
            'Reading SquashTM parameters from path/xYz failed',
            self.print_mock.call_args[0][0],
        )

    def test_TFParamService_get_param(self):
        squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
            'testSamples/global_and_test_test.ini'
        )
        self.assertEqual(squash_tf.TFParamService().getParam('TC4'), 'definedByTest')
        self.assertEqual(squash_tf.TFParamService().getParam('TC3'), 'definedByGlobal')
        self.assertEqual(squash_tf.TFParamService().getParam('foo'), None)

    def test_TFParamService_get_global_param(self):
        squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
            'testSamples/global_test.ini'
        )
        self.assertEqual(
            squash_tf.TFParamService().getGlobalParam('TC2'), 'definedByGlobal'
        )
        self.assertEqual(squash_tf.TFParamService().getGlobalParam('woof'), None)

    def test_TFParamService_get_test_param(self):
        squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
            'testSamples/test_test.ini'
        )
        self.assertEqual(
            squash_tf.TFParamService().getTestParam('TC4'), 'definedByTest'
        )
        self.assertEqual(squash_tf.TFParamService().getTestParam('woofwoof'), None)

    def test_TFParamService_get_test_param_nosubstitutions(self):
        squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
            'testSamples/test_test.ini'
        )
        self.assertEqual(squash_tf.TFParamService().getTestParam('TC5'), 'defined%TC4%')

    def test_TFParamService_get_test_param_nonamesubstitutions(self):
        squash_tf.squashTfRobotframeworkRunnerParameter._initialize_service(
            'testSamples/test_test.ini'
        )
        self.assertEqual(
            squash_tf.TFParamService().getTestParam('TC%7'), 'definedByYetAnotherTest'
        )


if __name__ == '__main__':
    unittest.main()
